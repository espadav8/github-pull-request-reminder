from github import Github
import os
from datetime import datetime, timedelta
from slackclient import SlackClient

github_token = os.environ.get('GITHUB_TOKEN', '')
repo_name = os.environ.get('GITHUB_REPO', '')
old_days = os.environ.get('GITHUB_OLD_IN_DAYS', 2)
stale_days = os.environ.get('GITHUB_STALE_IN_DAYS', 5)

slack_token = os.environ.get('SLACK_TOKEN', '')


github = Github(github_token)
slack_client = SlackClient(slack_token)


def run():
    repo = github.get_repo(repo_name)
    pulls = repo.get_pulls(state='open')
    old_date = datetime.now() - timedelta(days=old_days)
    stale_date = datetime.now() - timedelta(days=stale_days)

    old = []
    stale = []

    for pull in pulls:
        if pull.created_at < stale_date:
            stale.append(pull)
        elif pull.created_at < old_date:
            old.append(pull)

    for pull in old:
        send_message(pull, 'warning')

    for pull in stale:
        send_message(pull, 'failed')


def send_message(pull, icon='warning'):
    labels = ', '.join(l.name for l in pull.labels)
    age = datetime.now() - pull.created_at

    for x in pull.assignees:
        print(x)
    # print(r.name for r in reviewers)
    return

    slack_client.api_call(
            "chat.postMessage",
            channel="monolog-espadav8-dev",
            icon_emoji=":{}:".format(icon),
            blocks=[
                {
                    'type': 'section',
                    'text': {
                        'type': 'mrkdwn',
                        'text': '[{}] <{}|{}>'.format(labels, pull.url, pull.title)
                    }
                }, {
                    'type': 'context',
                    'elements': [
                        {
                            'type': 'mrkdwn',
                            'text': '{} days old'.format(age.days, pull.url, pull.title)
                        }
                    ]
                },
            ]
        )


def pretty_date(time=False):
    """
    Get a datetime object or a int() Epoch timestamp and return a
    pretty string like 'an hour ago', 'Yesterday', '3 months ago',
    'just now', etc
    """
    from datetime import datetime
    now = datetime.now()
    if type(time) is int:
        diff = now - datetime.fromtimestamp(time)
    elif isinstance(time,datetime):
        diff = now - time
    elif not time:
        diff = now - now
    second_diff = diff.seconds
    day_diff = diff.days

    if day_diff < 0:
        return ''

    if day_diff == 0:
        if second_diff < 10:
            return "just now"
        if second_diff < 60:
            return str(second_diff) + " seconds ago"
        if second_diff < 120:
            return "a minute ago"
        if second_diff < 3600:
            return str(second_diff / 60) + " minutes ago"
        if second_diff < 7200:
            return "an hour ago"
        if second_diff < 86400:
            return str(second_diff / 3600) + " hours ago"
    if day_diff == 1:
        return "Yesterday"
    if day_diff < 7:
        return str(day_diff) + " days ago"
    if day_diff < 31:
        return str(day_diff / 7) + " weeks ago"
    if day_diff < 365:
        return str(day_diff / 30) + " months ago"
    return str(day_diff / 365) + " years ago"

if __name__ == '__main__':
    run()
